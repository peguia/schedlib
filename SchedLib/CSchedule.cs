﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScheduleControls;

namespace ScheduleControls
{
    public class CSchedule
    {
        String _FechaInicio = "", _FechaFin = "";                               //Usamos una cadena de texto, porque tiene dia/mes pero sin año.
        bool _TodoElAño = false;                                                //Si todo el año es true, no hacemos caso de las fechas de inicio y fin
        //int _Dias = 0;                                                          //Usamos 0->todos los días, 1 Lunes, 2 Martes, 4 Miércoles, ... , 64 Domingo, 128 Festivos.
        bool _lunes = false, _martes = false, _miercoles = false, _jueves = false, 
            _viernes = false, _sabado = false, _domingo = false, _festivos = false;
        double[] _Valores = Enumerable.Repeat<double>(0.0, 24).ToArray();       //Array de 24 valores
        string _Nombre = "";                                                    //Un nombre que identifique el horario: diario, findesemana, festivos...
        int _Prioridad = -1;                                                    //Para la propiedad Prioridad: si es -1, está sin inicializar

        //Constructor Vacío
        public CSchedule() {  }
        //Constructor Copia
        public CSchedule(CSchedule s)  { Copiar(s); }
        //Constructor con valores parseados
        public CSchedule(double[] valores = null, int Id = -1, string fechaInicio = "", string fechaFin = "", string nombre="", int? prioridad = null) 
        {
            _Valores = (valores == null) ? _Valores : (double[]) valores.Clone();
            _FechaFin = fechaFin; _FechaInicio = fechaInicio;
            Prioridad = prioridad ?? -1;
            if ((fechaInicio == "") && (fechaFin == "")) _TodoElAño = true;
        }
        //Constructor con los valores de cadena, por ejemplo, de un xml
        public CSchedule(string horas, string valores, string dias = "", string fechaInicio = "", string fechaFin = "", string nombre="", int? prioridad = null)  
        {
            _Valores = SchedUtil.ParseHorarioEnCadena(horas, valores);
            interpretarDias(dias);
            _FechaFin = fechaFin; _FechaInicio = fechaInicio; _Nombre = nombre;
            if ((fechaInicio == "") && (fechaFin == "")) _TodoElAño = true;
            Prioridad = prioridad ?? -1;
        }
        public void Copiar(CSchedule s)
        {
            Lunes = s.Lunes; Martes = s.Martes; Miercoles = s.Miercoles; Jueves = s.Jueves; Viernes = s.Viernes; Sabado = s.Sabado; Domingo = s.Domingo; Festivos = s.Festivos;
            Nombre = s.Nombre; FechaInicio = s.FechaInicio; FechaFin = s.FechaFin; TodoElAño = s.TodoElAño;
            Prioridad = s.Prioridad;
            _Valores = (s._Valores == null) ? _Valores : (double[]) s._Valores.Clone();
        }
        public string Nombre { get { return _Nombre; } set { _Nombre = value; _CambioDeValores();  } }
        public int Prioridad { get { return _Prioridad; } set { _Prioridad = value; _CambioDeValores(); } }
        public bool Lunes { get { return _lunes; } set { _lunes = value; _CambioDeValores(); } }
        public bool Martes { get { return _martes; } set { _martes = value; _CambioDeValores(); } }
        public bool Miercoles { get { return _miercoles; } set { _miercoles = value; _CambioDeValores(); } }

        public bool Jueves { get { return _jueves; } set { _jueves = value; _CambioDeValores(); } }
        public bool Viernes { get { return _viernes; } set { _viernes = value; _CambioDeValores(); } }
        public bool Sabado { get { return _sabado; } set { _sabado = value; _CambioDeValores(); } }
        public bool Domingo { get { return _domingo; } set { _domingo = value; _CambioDeValores(); } }
        public bool Festivos { get { return _festivos; } set { _festivos = value; _CambioDeValores(); } }
        public string FechaInicio { get { return _FechaInicio; } set { _FechaInicio = value; _CambioDeValores(); } }
        public string FechaFin { get { return _FechaFin; } set { _FechaFin = value; _CambioDeValores(); } }
        public bool TodoElAño { get { return _TodoElAño; } set { _TodoElAño = value; _CambioDeValores(); } }
        
        public int MesDesde 
        {   get 
            {   if (TodoElAño == true) return 1;
                if (_FechaInicio == "") return 1;
                int iRes=1;
                if(int.TryParse(_FechaInicio.Split('/').ElementAt(1), out iRes)== true)
                    return iRes;
                else
                    return 1;
            } 
        }
        public int DiaDesde
        {
            get
            {
                if (TodoElAño == true) return 1;
                if (_FechaInicio == "") return 1;
                int iRes = 1;
                if (int.TryParse(_FechaInicio.Split('/').ElementAt(0), out iRes) == true)
                    return iRes;
                else
                    return 1;
            }
        }
        public int MesHasta
        {
            get
            {
                if (TodoElAño == true) return 12;
                if (_FechaFin == "") return 12;
                int iRes = 12;
                if (int.TryParse(_FechaFin.Split('/').ElementAt(1), out iRes) == true)
                    return iRes;
                else
                    return 12;
            }
        }
        public int DiaHasta
        {
            get
            {
                if (TodoElAño == true) return 31;
                if (_FechaFin == "") return 31;
                int iRes = 31;
                if (int.TryParse(_FechaFin.Split('/').ElementAt(0), out iRes) == true)
                    return iRes;
                else
                    return 31;
            }
        }
        public bool TodosLosDias { get { return (_lunes && _martes && _miercoles && _jueves && _viernes && _sabado && _domingo); } }
        public bool TodosLosDiasYFestivos { get { return (_lunes && _martes && _miercoles && _jueves && _viernes && _sabado && _domingo && _festivos); } }
        public string CadenaValores { get { return CadenaValoresHorario(); } }
        public string CadenaHoras { get { return CadenaHorasHorario();  } }
        public string CadenaDias { get { return CadenaDiasHorario();  } }

        private string CadenaValoresHorario() { return SchedUtil.CadenaValoresHorarios(_Valores); }
        private string CadenaHorasHorario() { return SchedUtil.CadenaHorasHorarios(_Valores); }
        private string CadenaDiasHorario()
        {
            if (TodosLosDiasYFestivos == true) { return "LMXJVSDF";  }
            if (TodosLosDias) return "LMXJVSD";
            
            string strRes = "";
            if (_lunes) strRes += "L"; if (_martes) strRes += "M"; if (_miercoles) strRes += "X"; if (_jueves) strRes += "J";
            if (_viernes) strRes += "V"; if (_sabado) strRes += "S"; if (_domingo) strRes += "D"; if (_festivos) strRes += "F";
            return strRes; 
        }
        public bool EsAplicable(DateTime fechaHora, bool festivo)
        {
            //Primero comprobar los festivos
            if (festivo == true && _festivos == false) return false;    //Desde fuera (o quizá dentro) hay que controlar si es un día festivo FALTA
            //Después, compruebo el día de la semana, a ver si no entra
            if (TodosLosDias == false)
            {
                switch (fechaHora.DayOfWeek)
                {
                    case DayOfWeek.Monday: if (_lunes == false) return false; break;
                    case DayOfWeek.Tuesday: if (_martes == false) return false; break;
                    case DayOfWeek.Wednesday: if (_miercoles == false) return false; break;
                    case DayOfWeek.Thursday: if (_jueves == false) return false; break;
                    case DayOfWeek.Friday: if (_viernes == false) return false; break;
                    case DayOfWeek.Saturday: if (_sabado == false) return false; break;
                    case DayOfWeek.Sunday: if (_domingo == false) return false; break;
                }
            }
            //Si todo el año es false, tengo que comprobar que el día está dentro de los meses y días permitidos
            if(TodoElAño == false)
            {
                if (fechaHora.Month < MesDesde) return false;
                if (fechaHora.Month > MesHasta) return false;
                if ((fechaHora.Month == MesDesde) && (fechaHora.Day < DiaDesde)) return false;
                if ((fechaHora.Month == MesHasta) && (fechaHora.Day > DiaHasta)) return false;
            }
            return true;
        }
        private void interpretarDias(string dias)
        {
            if(dias=="") {_lunes=true; _martes=true; _miercoles=true; _jueves=true; _viernes=true; _sabado=true; _domingo=true; _festivos=true; return;}

            _lunes = dias.Contains('L'); ; _martes = dias.Contains('M'); ; _miercoles = dias.Contains('X'); ;
            _jueves = dias.Contains('J'); ; _viernes = dias.Contains('V'); ; _sabado = dias.Contains('S'); ;
            _domingo = dias.Contains('D'); ; _festivos = dias.Contains('F'); ;
        }
        public double[] GetValoresHorarios() { return (double[]) _Valores.Clone(); }
        public void SetValoresHorarios(double[] d) { _Valores = (double[])d.Clone(); _CambioDeValores(); }
        public void SetValoresHorarios(double d) { _Valores = Enumerable.Repeat<double>(d, 24).ToArray<double>(); _CambioDeValores(); }

        private void _CambioDeValores()
        {

        }
    }
 
}
