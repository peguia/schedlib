﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleControls
{
    class ModXML
    {
        static public XmlDocument AbrirArchivoXML(string pathXML)
        {
            XmlDocument x=null;
            try
            {
                if (System.IO.File.Exists(pathXML))
                {
                    x = new XmlDocument();
                    x.Load(pathXML);
                }
            }
            catch (Exception) { }
            return x;
        }

        static public XmlNode LeerUnicoNodoXML(XmlNode nodo, string xpath)
        {
            XmlNode n;
            try
            {
                n = nodo.SelectSingleNode(xpath);
            }
            catch (Exception) { n = null; }
            return n;
        }
        static public XmlNodeList LeerListaNodosXML(XmlNode nodo, string xpath) 
        {
            XmlNodeList l;
            try
            {
                l = nodo.SelectNodes(xpath);
            }
            catch (Exception) { l = null; }
            return l;
        }
        static public double ValorNumericoXML(XmlNode nodo, double valor=0)
        {
            double d;
            try
            {
                return double.TryParse(nodo.Value, out d) ? d : valor;
            }
            catch (Exception) { return valor; }
        }
        static public string ValorCadenaXML(XmlNode nodo, string cadena = "")
        {
            try
            {
                return (nodo.InnerText == null) ? cadena : nodo.InnerText;
            }
            catch (Exception) { return cadena; }
        }
        static public string LeerValorUnicoNodoCadena(XmlNode nodo, string xpath, string cadena= "")
        {
            XmlNode n = LeerUnicoNodoXML(nodo, xpath);
            return ValorCadenaXML(n, cadena);
        }
        static public int LeerValorUnicoNodoEntero(XmlNode nodo, string xpath, int valor=0)
        {
            XmlNode n = LeerUnicoNodoXML(nodo, xpath);
            int retValue;
            if(Int32.TryParse(n.InnerText, out retValue)==true)
                return retValue;
            else
                return valor;
        }
    }
}
