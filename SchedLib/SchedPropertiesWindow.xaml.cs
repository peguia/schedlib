﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScheduleControls
{
    /// <summary>
    /// Interaction logic for SchedPropertiesWindow.xaml
    /// </summary>
    public partial class SchedPropertiesWindow : Window
    {
        public CSchedule MiHorario;
        public SchedPropertiesWindow()
        {
            InitializeComponent();
            MiHorario = new CSchedule();
            this.DataContext = MiHorario;
        }
        public SchedPropertiesWindow(CSchedule s)
        {
            InitializeComponent();
            MiHorario = new CSchedule(s);
            this.DataContext = MiHorario;
        }
        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
    
}
