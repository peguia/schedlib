﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScheduleControls
{
    /// <summary>
    /// Interaction logic for SchedSetButtonCtrl.xaml
    /// </summary>
    public partial class SchedSetButtonCtrl : UserControl
    {
        SchedSetCtrl MiSchedSet;
        string[] _ColoresFondo = { "#FF0D58A6", "#ff00B358", "#ffBF5230", "#FFB19C7D", "#FFFF9900", "#FF4188D2", "#FF36D986", "#ffFF6E40", "#ffFFB240" };
        CSchedule _Horario;

        //Propiedad NumColor
        static FrameworkPropertyMetadata propertymetadata = new FrameworkPropertyMetadata(1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal, new PropertyChangedCallback(NumColor_PropertyChanged),
            new CoerceValueCallback(NumColor_CoerceValue), false, UpdateSourceTrigger.PropertyChanged);

        public static DependencyProperty NumColorProperty = DependencyProperty.Register("NumColor", typeof(Nullable<int>), typeof(SchedSetButtonCtrl), propertymetadata);
        private static void NumColor_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
            SchedSetButtonCtrl b = (SchedSetButtonCtrl)dobj;
            Nullable<int> value = (Nullable<int>)e.NewValue;
            if (value != null)
            {
               b.SetValue(NumColorProperty, value); 
            }
            else
            {
                b.SetValue(NumColorProperty, 0);
            }
            b.SetColor((int) b.GetValue(NumColorProperty));
        }

        private static object NumColor_CoerceValue(DependencyObject dobj, object Value)
        {
            //called whenever dependency property value is reevaluated. The return value is the
            //latest value set to the dependency property
            return Value;
        }
        public SchedSetButtonCtrl()
        {
            InitializeComponent();
            Dibujar();
        }
        public Nullable<int> NumColor
        {
            get { return (int) this.GetValue(NumColorProperty); }
            set { this.SetValue(NumColorProperty, value); Dibujar(); }
        }
        public void SetSchedSet(SchedSetCtrl s) { MiSchedSet = s; }
        public void SetSchedule(CSchedule horario) //Si es null, el botón se pone en casi transparente. Si no, sabe los datos del horario (días, horas, ...)
        {
            _Horario = horario;
            Dibujar();
        }
        public void SetColor(int NumeroColor) {
            SolidColorBrush c = new SolidColorBrush((Color)ColorConverter.ConvertFromString(_ColoresFondo[NumeroColor-1]));
            MyRectangle.Fill = c; MyRectangle2.Fill = c;
            if(MiSchedSet!=null)  MiSchedSet.SetNumeroStyle(NumeroColor);
        }
        private void Encender()
        {
            MyRectangle.Opacity = 1; MyRectangle2.Opacity = 1; MyRectangle3.Opacity = 0.4;
        }
        private void Apagar()
        {
            MyRectangle.Opacity = 0.1; MyRectangle2.Opacity = 0.1; MyRectangle3.Opacity = 0.1;
        }
        public void SetActivo(Boolean b)
        {
            if (_Horario == null) Apagar(); else Encender();
            if (b) 
            {
                MyRectangle3.Opacity = 0.4;
            }
            else
            {
                MyRectangle3.Opacity = 1;
            }

        }
        private void Dibujar()
        {
            if (_Horario == null) Apagar(); else Encender();
            SetColor((int)GetValue(NumColorProperty));
        }

        private void BtnNuevoSched_Click(object sender, RoutedEventArgs e)
        {
            MiSchedSet.AñadirHorario(new CSchedule());

        }

        private void MiButtonGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (MiSchedSet == null) return;
            if (_Horario == null) return;
            MiSchedSet.ActivarHorario(this);
        }
    }
}
