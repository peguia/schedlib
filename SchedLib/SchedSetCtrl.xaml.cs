﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScheduleControls
{
    /// <summary>
    /// Interaction logic for SchedSetCtrl.xaml
    /// </summary>
    public delegate void SchedSetCtrlChanged(object SchedSetCtrlSender, object SchedSet);
    public delegate void SchedSetCtrlDelete(object SchedSetCtrlSender);
    public partial class SchedSetCtrl : UserControl
    {
        CSchedSet MiSchedSet;
        List<SchedSetButtonCtrl> _Botones;
        CSchedule _ActiveSched;

        public event SchedSetCtrlChanged Changed;
        public event SchedSetCtrlDelete DeleteRequest;

        ~SchedSetCtrl()
        {
            //Para desvincularnos del SchedSet en el que podemos estar referenciados
            if (MiSchedSet != null) MiSchedSet.DesRegistrar(this);
        }
        public SchedSetCtrl(CSchedSet s)
        {
            InitializeComponent();
            MiSchedSet = s;
            MiSchedSet.Registrar(this);
            this.DataContext = MiSchedSet;
            InicializarControles();
            if(Changed!=null) Changed(this, MiSchedSet);
        }
        public SchedSetCtrl()
        {
            InitializeComponent();
            MiSchedSet = new CSchedSet();
            MiSchedSet.Registrar(this);
            this.DataContext = MiSchedSet;
            InicializarControles();
            if (Changed != null) Changed(this, MiSchedSet);
        }

        public CSchedSet ActualSchedSet 
        {   get { return MiSchedSet; }
            set
            {
                if (MiSchedSet != null) MiSchedSet.DesRegistrar(this);  //Nos quitamos del anterior CSchedSet;
                MiSchedSet = value; this.DataContext = MiSchedSet; 
                InicializarControles();
                MiSchedSet.Registrar(this);                             //Nos añadimos al actual CSchedSet;
                if (Changed != null) Changed(this, MiSchedSet); 
            }
        }

        public void InvalidarControl()
        {
            InicializarControles();
            //Debería mostrar lo que ya está mostrando y no inicializar TODO el control.
        }
        private void InicializarControles()
        {
            int i, j;

            if (_ActiveSched != null) MiSchedGrid.SetValoresHorarios(_ActiveSched.GetValoresHorarios());
            MiSchedGrid.SetSchedSet(this);
            
            _Botones = (List<SchedSetButtonCtrl>) GridBotones.Children.OfType<SchedSetButtonCtrl>().ToList();
            foreach (SchedSetButtonCtrl b in _Botones) b.SetSchedSet(this);
            for( i = 0; i < MiSchedSet.NumeroDeHorarios; i++) _Botones[i].SetSchedule(MiSchedSet.GetHorario(i));
            for( j = i; j< _Botones.Count(); j++) _Botones[j].SetSchedule(null);

            int? k = GetIndiceHorarioActivo(); 
            if (MiSchedSet.NumeroDeHorarios > 0 && k == -1) k = 0; else k = null;
            ActivarHorario(k); 
            MiTitulo.Text = MiSchedSet.Nombre;
        }
        public void AñadirHorario(CSchedule s, bool activarlo = true)
        {
            int index = MiSchedSet.NumeroDeHorarios;
            _Botones.ElementAt<SchedSetButtonCtrl>(index).SetSchedule(s);
            MiSchedSet.AñadirHorario(s);
            
            MiSchedGrid.SetValoresHorarios(s);
            if (activarlo) ActivarHorario(index);
            if (Changed != null) Changed(this, MiSchedSet);
            MiSchedSet.AvisarUserControls(this);
        }
        
        public void BorrarHorarioActivo()
        {
            if (_ActiveSched == null) return;
            try
            {
                int i = MiSchedSet.IndexOf(_ActiveSched);
                MiSchedSet.BorrarHorario(i);
                ReleerListaHorarios(i);
                if (Changed != null) Changed(this, MiSchedSet);
                MiSchedSet.AvisarUserControls(this);
            }
            catch (Exception) { }
        }

        public void ReleerListaHorarios(int indexActivo)
        {
            int i;
            for (i = 0; i < _Botones.Count; i++)
            {
                if(i<MiSchedSet.NumeroDeHorarios)
                    _Botones[i].SetSchedule(MiSchedSet.GetHorario(i));
                else
                    _Botones[i].SetSchedule(null);
            }
            if (MiSchedSet.NumeroDeHorarios == 0)
            {
                _ActiveSched = null;
                MiSchedGrid.SetValoresHorarios((CSchedule) null);
                return;
            }
            if (indexActivo < MiSchedSet.NumeroDeHorarios)
                ActivarHorario(indexActivo);
            else if(indexActivo>0)
                ActivarHorario(indexActivo - 1);
        }

        public void ActivarHorario(SchedSetButtonCtrl b)
        {
            ActivarHorario(_Botones.IndexOf(b));
        }
        private SchedSetButtonCtrl GetBotonActivo()
        {
            if (_ActiveSched == null) return null;
            return _Botones.ElementAt(MiSchedSet.IndexOf(_ActiveSched));
        }
        public int GetIndiceHorarioActivo() { if (_ActiveSched == null) return -1; else return MiSchedSet.IndexOf(_ActiveSched); }
        public void ActivarHorario(int? indexActivo)    //Si pasamos null redibuja con el horario activo actual
        {
            int indice = indexActivo ?? GetIndiceHorarioActivo();
            if (indice == -1) { ApagarControl(); return; } else EncenderControl();
            _ActiveSched = MiSchedSet.GetHorario(indice);
            MiPie.Text = "Nombre: " + _ActiveSched.Nombre +  " - Válido para: " + _ActiveSched.CadenaDias;
            SetNumeroStyle((int)_Botones[indice].NumColor);
            EncenderBoton(indice);
            MiSchedGrid.SetValoresHorarios(_ActiveSched.GetValoresHorarios());
        }
        private void EncenderBoton(int index)
        {
            for (int i = 0; i < MiSchedSet.NumeroDeHorarios; i++)
            {
                if (i == index)
                    _Botones[i].SetActivo(true);
                else
                    _Botones[i].SetActivo(false);
            }
        }
        public void ApagarControl()
        {
            MiPie.Text = "..."; //MiTitulo.Text = "...";
            ApagarTodosLosBotones();
            MiSchedGrid.BorrarValoresHorarios();
            MiSchedGrid.Editable = false;
        }
        public void EncenderControl()
        {
            MiSchedGrid.Editable = true;
        }
        private void ApagarTodosLosBotones() { foreach (SchedSetButtonCtrl b in _Botones) b.SetActivo(false); }

        public void ActualizarValoresHorario(SchedGridCtrl s)
        {
            if (_ActiveSched != null) { _ActiveSched.SetValoresHorarios(s.GetValoresHorarios()); if (Changed != null) Changed(this, MiSchedSet); MiSchedSet.AvisarUserControls(this); }
        }
        public void SetNumeroStyle(int numero)
        {
            MiSchedGrid.SetNumeroStyle(numero);
        }

        private void MiSchedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(e.ChangedButton == MouseButton.Left) MiSchedGrid.ToggleEditar();
        }

        private void BtnNuevoSched_Click(object sender, RoutedEventArgs e)
        {
            if (MiSchedSet.NumeroDeHorarios < 9) { AñadirHorario(new CSchedule()); Changed(this, MiSchedSet); }
        }

        private void BtnPropSched_Click(object sender, RoutedEventArgs e)
        {
            if (_ActiveSched == null) return;
            SchedPropertiesWindow w = new SchedPropertiesWindow(_ActiveSched);
            w.Left = BtnPropSched.PointToScreen(new Point(0, 0)).X + BtnPropSched.ActualWidth;
            w.Top = BtnPropSched.PointToScreen(new Point(0, 0)).Y;
            if (w.ShowDialog() == true)
            {
                _ActiveSched.Copiar(w.MiHorario);
                ActivarHorario((int?) null);
                if (Changed != null) Changed(this, MiSchedSet);
            }
        }

        private void BtnBorrarSched_Click(object sender, RoutedEventArgs e)
        {
            if (_ActiveSched != null) { BorrarHorarioActivo(); if (Changed != null) Changed(this, MiSchedSet); }
        }

        private void BtnBorrarSchedSet_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteRequest != null) DeleteRequest(this);
        }

    }
}
