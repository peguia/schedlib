﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace ScheduleControls
{
    /// <summary>
    /// Interaction logic for SchedHourCtrl.xaml
    /// </summary>
    public partial class SchedHourCtrl : UserControl
    {
        public SchedGridCtrl MiGrid;
        public double MinY=0, MaxY=1;
        private double MyValor;
        private bool _SettingValue = false;

        public SchedHourCtrl()
        {
            InitializeComponent();
            MiLineaResizear.SetSchedCtrl(this);
        }
        public void ShowResizeAdorner()  {  MiLineaResizear.Visibility = Visibility.Visible; }
        public void HideResizeAdorner() { MiLineaResizear.Visibility = Visibility.Hidden; }
        public void HideLabelValor() { lbValor.Visibility = Visibility.Hidden; }
        public void ShowLabelValor() { lbValor.Visibility = Visibility.Visible; }
        public void SetGrid(SchedGridCtrl g) { MiGrid = g; }
        public void SetSchedHourStyle(Style s) { this.SchedHourCanvas.Style = s; }
        public void SetMaxMinValues(double y_max, double y_min) {
            MinY = y_min; MaxY = y_max;
            //Por si el valor cae fuera, establecemos el valor igual al que tiene y que se corrija
            SetValor(MyValor, false);
            SetHeightControl(MyValor, false);
        }
        public void SetValor(double valor, bool vecinos = true, int lado=-1)  //Podemos tener problemas con que vayamos a cambiar a un vecino que nos mandó cambiar a nosotros
        {   
            if(_SettingValue==false)
            {
                _SettingValue = true;
                double dblValor = Math.Max(MinY, Math.Min(MaxY, valor));    //Ver si el nuevo valor está dentro de los límites

                SetHeightControl(dblValor, vecinos, lado);                  //Poner el valor a los vecinos antes de cambiar el mío (si no, puede que no encuentre vecinos)
                MyValor = dblValor;                                         //Pongo mi valor
                SetCaptionAdorno();                                         //Pongo el valor en el caption
            }
            _SettingValue = false;
        }
        public double GetValor() { return MyValor; }
        private void SetCaptionAdorno() { lbValor.Content = MyValor.ToString("0.00"); }
        private double CalcularCambioAltura(double dblValorNuevo)
        {
            double MaximaAltura = this.ActualHeight;
            if (MaximaAltura == 0) return 0.0;
            double AlturaNueva = MaximaAltura * dblValorNuevo / MaxY;
            return (double) AlturaNueva - MiRectangulo.ActualHeight;
        }
        public void ResizearControl(double deltaHeight, int lado=-1)
        {
            Canvas c = this.SchedHourCanvas;
            Rectangle r = this.MiRectangulo;
            if ((c != null) && (r != null))
            {
                Canvas.SetTop(c, Canvas.GetTop(c) + deltaHeight);
                Canvas.SetTop(r, Canvas.GetTop(r) + deltaHeight);
                r.Height = r.ActualHeight - deltaHeight;
            }
            if (MiGrid != null)
            {
                MiGrid.ResizearVecinos(this, deltaHeight, lado);
            }
            MyValor = (r.ActualHeight / this.ActualHeight) * (MaxY - MinY) +  MinY;
            MiGrid.ResizeHour_Delta(this);
            SetCaptionAdorno();
        }
        public void SetHeightControl(double newHeight, bool vecinos = true, int lado=-1)    //En realidad, la altura es el valor y no la altura del control. Tener cuidado!
        {
            if (MiGrid == null) return;
            if (MaxY == MinY) return;

            double dblNewHeight = (newHeight - MinY) / (MaxY - MinY) * SchedHourCanvas.ActualHeight;
            double dblTop = SchedHourCanvas.ActualHeight - dblNewHeight;
            Canvas c = this.SchedHourCanvas;
            Rectangle r = this.MiRectangulo;
            if ( (c != null) && (r != null))
            {
                Canvas.SetTop(c, dblTop);
                Canvas.SetTop(r, dblTop);
                r.Height = dblNewHeight;
            }
            if (MiGrid!=null && vecinos==true)
            {
                MiGrid.SetHeightVecinos(this, newHeight, lado);
            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetValor(MyValor, false);
        }

        public void ResizeThumb_DragStarted(object sender, DragStartedEventArgs e) 
        {
            MiGrid.ResizeHour_Start(this);
        }
        public void ResizeThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            MiGrid.ResizeHour_Completed(this);
        }

        private void _InterpretarSchedHourWindow(SchedHourWindow w)
        {
            double dblValorNuevo;
            if (double.TryParse(w.TxtValor.Text, out dblValorNuevo) == false) return;
            if (dblValorNuevo > MaxY) dblValorNuevo = MaxY; else if (dblValorNuevo < MinY) dblValorNuevo = MinY;
            if (w.RBTodo.IsChecked == true) { MiGrid.SetValoresHorarios(dblValorNuevo); return; }
            if (w.RBSolo.IsChecked == true) { SetValor(dblValorNuevo, false); MiGrid.ResizeHour_Completed(this); return; }
            
            int lado = -1;
            if (w.RBAmbos.IsChecked == true) lado = 0;
            if (w.RBIzquierda.IsChecked == true) lado = 1;
            if (w.RBDerecha.IsChecked == true) lado = 2;
            SetValor(dblValorNuevo, true, lado);
            if (MiGrid != null) MiGrid.ResizeHour_Completed(this);
        }

        private void MiLineaResizear_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            SchedHourWindow w = new SchedHourWindow();
            w.TxtValor.Text = MyValor.ToString();
            if (w.ShowDialog() == true) _InterpretarSchedHourWindow(w);
        }
    }

    
    public class LineaResizear : Thumb
    {
        //Esta clase permite resizear todo el control. Si el rectángulo cambia de tamaño, llama a esta clase
        //Cuando el usercontrol cambie de tamaño llamará al SchedHourCtrl que, a su vez, llamará a LineaResizear.
        //Existen 2 funciones: con ResizearThumb se le pasa el incremento en vertical para el resizeo.
        //y con SetHeight se le pasa la altura absoluta
        SchedHourCtrl MiSchedHourCtrl;
        public LineaResizear()
        {
            DragDelta += new DragDeltaEventHandler(this.ResizeThumb_DragDelta);
            DragStarted += new DragStartedEventHandler(this.ResizeThumb_DragStarted);
            DragCompleted += new DragCompletedEventHandler(this.ResizeThum_DragCompleted);            
        }
        
        public void SetSchedCtrl(SchedHourCtrl s) { MiSchedHourCtrl = s; }
        private void ResizeThumb_DragStarted(object sender, DragStartedEventArgs e) 
        {
            MiSchedHourCtrl.ResizeThumb_DragStarted(sender, e);
        }
        private void ResizeThum_DragCompleted(object sender, DragCompletedEventArgs e) 
        {
            MiSchedHourCtrl.ResizeThumb_DragCompleted(sender, e);
        }
        private void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (MiSchedHourCtrl == null) return;
            double dblMinHeight = MiSchedHourCtrl.MiRectangulo.MinHeight;
            double dblActualHeight = MiSchedHourCtrl.MiRectangulo.ActualHeight;
            double deltaVertical;
            deltaVertical = Math.Min(e.VerticalChange, dblActualHeight - dblMinHeight);
            double dblMaxHeight = MiSchedHourCtrl.ActualHeight;
            deltaVertical = Math.Max(deltaVertical, dblActualHeight - dblMaxHeight);
            ResizearThumb(deltaVertical);
            e.Handled = true;
        }
        //ResizearThumb se emplea desde fuera para resizear varios SchedHourCtrl a la vez
        //Cuando un SchedHourCtrl quiere resizear a sus vecinos, llama a ResizearThumb y no a ResizearControl
        public void ResizearThumb(double deltaVertical, int lado=-1)
        {
            if (MiSchedHourCtrl == null) return;
            MiSchedHourCtrl.ResizearControl(deltaVertical, lado);
        }
    
    }

}
