﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleControls
{
    class ModXMLHorarios
    {
        static public List<CSchedule> LeerArchivoHorariosXML(string path, string TipoEdificio = "", string TipoZona= "", string TipoHorario = "")
        {
            List<CSchedule> l = null;
            try
            {
                XmlDocument d = ModXML.AbrirArchivoXML(path);
                if (d != null)
                {
                    string strXPath = XPathNodosHorariosXML(TipoEdificio, TipoZona, TipoHorario);
                    XmlNodeList listaNodos = ModXML.LeerListaNodosXML(d.DocumentElement, strXPath);
                    l = HorariosXML2Lista(listaNodos);
                }
            }
            catch (Exception) { };
            if (l == null) l = new List<CSchedule>();  //Devuelve una lista vacía y no null
            return l;
        
        }
        static public List<CSchedule> HorariosXML2Lista(XmlNodeList horarios)
        {
            List<CSchedule> l = new List<CSchedule>();
            foreach(XmlNode n in horarios)
            {
                string nombre, fechainicio, fechafin, dias, horas, valores;
                //nombre = ModXML.LeerValorUnicoNodoCadena(n, "TipoZona", "[...]");
                XmlNodeList listavalores = ModXML.LeerListaNodosXML(n, XPathValoresHorarios());
                foreach (XmlNode h in listavalores)
                {
                    LeerValoresHorarioXML(h, out nombre, out horas, out valores, out dias, out fechainicio, out fechafin);
                    CSchedule s = new CSchedule(horas, valores, dias, fechainicio, fechafin, nombre);
                    l.Add(s);
                }
            }
            return l;
        }
        static public void LeerValoresHorarioXML(XmlNode n, out string nombre, out string horas, out string valores, out string dias, out string fechainicio, out string fechafin)
        {
            nombre = ModXML.LeerValorUnicoNodoCadena(n, "Nombre");
            horas = ModXML.LeerValorUnicoNodoCadena(n, "Horas");
            valores = ModXML.LeerValorUnicoNodoCadena(n, "Valores");            
            dias = ModXML.LeerValorUnicoNodoCadena(n, "Dias");
            fechainicio = ModXML.LeerValorUnicoNodoCadena(n, "FechaInicio");
            fechafin = ModXML.LeerValorUnicoNodoCadena(n, "FechaFin");
        }
        static public string XPathNodosHorariosXML(string TipoEdificio="", string TipoZona= "", string TipoHorario ="")
        {
            string strXPath;
            if (TipoEdificio != "")
            {
                if (TipoZona != "")
                {
                    if (TipoHorario != "")  strXPath = XPathHorariosTipoEdificioZonaYHorario(TipoEdificio, TipoZona, TipoHorario); 
                    else strXPath = XPathHorariosTipoEdificioYZona(TipoEdificio, TipoZona); 
                }
                else
                {
                    if (TipoHorario != "") strXPath = XPathHorariosTipoEdificioYHorario(TipoEdificio, TipoHorario);
                    else strXPath = XPathHorariosTipoEdificio(TipoEdificio);
                }
                
            }
            else
            {
                if (TipoZona != "")
                {
                    if (TipoHorario != "") strXPath = XPathHorariosTipoZonaYHorario(TipoZona, TipoHorario);
                    else strXPath = XPathHorariosTipoZona(TipoZona);
                }
                else
                {
                    if (TipoHorario != "") strXPath = XPathHorariosTipoHorario(TipoHorario);
                    else strXPath = "GrupoHorarios/Horario";
                }
                
            }
            return strXPath;
        }
        static public string XPathHorariosTipoEdificio(string TipoEdificio) { return "Horario[../TipoEdificio='" + TipoEdificio + "']"; }
        static public string XPathHorariosTipoZona(string TipoZona) { return "Horario[TipoZona=\'" + TipoZona + "']"; }
        static public string XPathHorariosTipoHorario(string TipoHorario) { return "Horario[TipoHorario='" + TipoHorario + "']"; }
        static public string XPathHorariosTipoZonaYHorario(string TipoZona, string TipoHorario) { return "Horario[TipoZona='" + TipoZona + "' and TipoHorario='" + TipoHorario + "']"; }
        static public string XPathHorariosTipoEdificioYHorario(string TipoEdificio, string TipoHorario) { return "Horario[../TipoEdificio='" + TipoEdificio + "'  and TipoHorario='" + TipoHorario + "']"; }
        static public string XPathHorariosTipoEdificioZonaYHorario(string TipoEdificio, string TipoZona, string TipoHorario) { return "Horario[../TipoEdificio='" + TipoEdificio + "' and TipoHorario='" + TipoHorario + "' and TipoZona='" + TipoZona + "']"; }
        static public string XPathHorariosTipoEdificioYZona(string TipoEdificio, string TipoZona) { return "Horario[../TipoEdificio='" + TipoEdificio + "' and TipoZona='" + TipoZona + "']"; }
        static public string XPathValoresHorarios() { return "ValoresHorario"; }



    }
}
