﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScheduleControls
{
    /// <summary>
    /// Interaction logic for SchedOptions.xaml
    /// </summary>
    public partial class SchedOptionsWindow : Window
    {
        public CSchedSet MiSchedSet;
        public SchedOptionsWindow()
        {
            InitializeComponent();
            MiSchedSet = new CSchedSet();
            this.DataContext = MiSchedSet;
        }

        public SchedOptionsWindow(CSchedSet s)
        {
            InitializeComponent();
            MiSchedSet = new CSchedSet();
            MiSchedSet.Copiar(s);
            this.DataContext = MiSchedSet;
        }

        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
