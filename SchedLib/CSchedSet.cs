﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ScheduleControls
{
    public enum TiposHorarios { Infiltracion, Iluminacion, VentilacionCaudal, VentilacionTemperatura, VentilacionHumedad, Equipos, ConfortRopa, ConfortMetabolismo, 
        ConfortVentilacion, Ocupacion, ConsignaCalor, ConsignaFrio, PotenciaCalor, PotenciaFrio, DistribucionCal, DistribucionRef, ProduccionCal, ProduccionRef, NoAsignado }
    
    public class CSchedSet : INotifyPropertyChanged
    {
        public List<CSchedule> _Horarios = new List<CSchedule>();
        public bool AvisarControles = true;
        private List<SchedSetCtrl> _Controles = new List<SchedSetCtrl>();  //Para invalidar a los controles cuando ha cambiado sin usar eventos
        private String _Nombre = "";
        private Boolean _NotacionExp = false;
        private double _min = 0.0, _max = 1.0;
        private int _NumDecimales = 2;                                     //Lo que se usa para el ejeY se debe tener en cuenta para sacar el archivo
        private TiposHorarios _TipoHorario;                                //En realidad, debería ser una Enumeration
        int _Id = -1;

        public event PropertyChangedEventHandler PropertyChanged;

        public CSchedSet() { }                                             //Constructor vacío
        public CSchedSet(string path, string TipoEdificio = "", string TipoZona = "", string TipoHorario = "")    //Se construye a través de un archivo xml con los horarios
        {
            try {
                _Horarios = ModXMLHorarios.LeerArchivoHorariosXML(path, TipoEdificio, TipoZona, TipoHorario);
                _Nombre = TipoZona;
            }
            catch(Exception) {}            
        }
        public CSchedSet(CSchedSet s) { Copiar(s); }                       //Constructor copia

        public String Nombre { get { return _Nombre; } set { _Nombre = value; OnPropertyChanged("Nombre"); } }
        public Boolean NotacionExponencial { get { return _NotacionExp; } set { _NotacionExp = value; OnPropertyChanged("NotacionExponencial"); } }
        public double MaxValue { get { return _max; } set { _max = value; _ComprobarValorMaximo(); OnPropertyChanged("MaxValue"); } }
        public double MinValue { get { return _min; } set { _min = value; _ComprobarValorMinimo(); OnPropertyChanged("MinValue"); } }
        public int NumeroDecimales { get { return _NumDecimales; } set { _NumDecimales = value; OnPropertyChanged("NumeroDecimales"); } }
        public TiposHorarios TipoHorario { get { return _TipoHorario; } set { _TipoHorario = value; OnPropertyChanged("TipoHorario"); } }
        public int Id { get { return _Id; } set { _Id = value; OnPropertyChanged("Id"); } }

        public void Registrar(SchedSetCtrl s) {if (_ControlRegistrado(s) == false) _Controles.Add(s); }
        public void DesRegistrar(SchedSetCtrl s) {if(_ControlRegistrado(s) == true) _Controles.Remove(s); }
        private bool _ControlRegistrado(SchedSetCtrl s) { return _Controles.Contains(s); }

        protected void OnPropertyChanged(string name)
        {
              PropertyChangedEventHandler handler = PropertyChanged;
              if (handler != null)
              {
                  handler(this, new PropertyChangedEventArgs(name));
              }
              if(AvisarControles==true) AvisarUserControls();
        }

        public void AvisarUserControls(SchedSetCtrl sender = null) 
        {
            if (sender == null)
                foreach (SchedSetCtrl s in _Controles) s.InvalidarControl();
            else
                foreach (SchedSetCtrl s in _Controles)
                    if (s != sender) s.InvalidarControl();
        }
        public void AñadirHorario(CSchedule s) 
        {
            if (s.Prioridad == -1) { s.Prioridad = _Horarios.Count; }
            _Horarios.Add(s); 
        }
        public CSchedule GetHorario(int index) { return _Horarios.ElementAt<CSchedule>(index);}
        public void BorrarHorario(int index) { _Horarios.RemoveAt(index); }
        public int NumeroDeHorarios { get { return _Horarios.Count(); } }
        public int IndexOf(CSchedule s) { return _Horarios.IndexOf(s); }

        public void Copiar(CSchedSet s, bool CopiarValoresHorarios = true)
        {
            //Parar los eventos
            this.AvisarControles = false;

            //Con el Select, copiamos todos los CSchedule de la lista y con dichas copias creamos una nueva lista
            if (CopiarValoresHorarios == true) // A veces, queremos copiar los datos del CSchedSet pero no los valores de los CSchedules
            {
                if (s._Horarios != null)
                    _Horarios = new List<CSchedule>(s._Horarios.Select(x => new CSchedule(x)));
                else _Horarios = null;
            }
            Nombre = s.Nombre;
            NotacionExponencial = s.NotacionExponencial;
            MaxValue = s.MaxValue;
            MinValue = s.MinValue;
            NumeroDecimales = s.NumeroDecimales;
            Id = s.Id;
            //Volver a poner eventos
            this.AvisarControles = true;
            
            //Avisar a los controles
            AvisarUserControls();
        }

        //GetValoresHorarios devuelve un array con los valores hora a hora para que se le pueda pasar a un archivo
        public double[] GetValoresHorarios(DateTime fechaDesde, DateTime fechaHasta)
        {
            TimeSpan TS = fechaHasta.Subtract(fechaDesde);
            double[] valoresRes = new double[(int) TS.TotalHours];

            for (int i = 0; i <= valoresRes.GetUpperBound(0); i++)
            {
                DateTime fecha = fechaDesde.AddHours(i);
                bool festivo = false;
                valoresRes[i] = _valorSchedSet(fecha, festivo);
            }
            return valoresRes;
        }
        private double _valorSchedSet(DateTime fechaHora, bool festivo)
        {
            CSchedule schedPrioritario = _HorarioPrioritario(fechaHora, festivo);
            return schedPrioritario.GetValoresHorarios()[fechaHora.Hour]; 
        }
        private CSchedule _HorarioPrioritario(DateTime fechaHora, bool festivo)
        {
            if (_Horarios.Count == 0) return null;
            for (int i = _Horarios.Count - 1; i >= 0; i--)
                if (_Horarios[i].EsAplicable(fechaHora, festivo) == true) return _Horarios[i];            
            return _Horarios[0];
        }

        private void _ComprobarValorMinimo()
        {
            bool bCambios;
            foreach(CSchedule s in _Horarios)
            {
                bCambios = false;
                double[] v = s.GetValoresHorarios();
                for (int i = 0; i < 24; i++)
                    if (v[i] < _min) { v[i] = _min; bCambios = true; }
                if(bCambios == true) s.SetValoresHorarios(v);
            }
        }

        private void _ComprobarValorMaximo()
        {
            foreach (CSchedule s in _Horarios)
            {
                double[] v = s.GetValoresHorarios();
                for (int i = 0; i < 24; i++)
                    if (v[i] > _max) v[i] = _max;
            }

        }
    }
}
