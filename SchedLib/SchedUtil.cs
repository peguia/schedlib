﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ScheduleControls
{
    public static class SchedUtil
    {
        //Esta función recibe 2 cadenas: una con horas tipo "0-8,8-14,14-17, 17-24" y otra con los valores para esos intervalos
        //de horas "0.0, 0.8, 0.6, 0.1"
        //Por supuesto, el número de valores tiene que coincidir con el de intervalos
        //Devuelve un array de 24 valores double para que los controles dibujen ese horario de un día
        //Cualquier error, devuelve un array de 24 ceros
        public static double[] ParseHorarioEnCadena(string horas, string valores)
        {
            double[] returnValues = Enumerable.Repeat<double>(0.0, 24).ToArray();
            int iNumHoras, iHoraIni, iHoraFin;
            try
            {
                String[] MisValores = valores.Split(',');
                String[] MisHoras = horas.Split(','); if ((MisHoras.Count() == 1) && (MisHoras[0] == "")) MisHoras[0] = "0-23";
                if (MisValores.Count() == 0 || (MisValores.Count() != MisHoras.Count())) return returnValues;
                int contadorHora = 0;
                for (int i = 0; i < MisValores.Count(); i++)
                {
                    String[] h = MisHoras[i].Split('-');
                    if (h.Count() == 1)
                    {
                        iNumHoras = 1;
                        int.TryParse(h[0], out iHoraIni); iHoraFin = iHoraIni; 
                    }
                    else
                    {
                        int.TryParse(h[0], out iHoraIni); int.TryParse(h[1], out iHoraFin);
                        iNumHoras = iHoraFin - iHoraIni + 1;
                    }
                    
                    for (int j = 0; j < iNumHoras; j++) { returnValues[contadorHora + j] = ParseValorHorario(MisValores[i]); }
                    contadorHora += iNumHoras;
                }
                return returnValues;
            }
            catch (Exception)
            {
                return returnValues;
            }
        }


        public static string CadenaHorasHorarios(double[] valores)
        {
            string strRes="";
            int indice = 0, i;
            for (i = 1; i < 24; i++)
            {
                if ((valores[i] != valores[indice]))
                {
                    if (indice > 0) strRes += ",";
                    if (indice == (i - 1))
                    {
                        strRes += indice.ToString();
                    }
                    else
                    {
                        strRes += indice.ToString() + "-" + (i - 1).ToString();
                    }
                    indice = i; 
                }
                if (i == 23)
                {
                    if (indice > 0) strRes += ",";
                    if (indice == 23) { strRes += "23"; }
                    else { strRes += indice.ToString() + "-23"; }
                }
            }
            return strRes;
        }

        public static string CadenaValoresHorarios(double[] valores)
        {
            string strRes = "";
            int indice = 0, i;
            for (i = 1; i < 24; i++)
            {
                if ((valores[i] != valores[indice]))
                {
                    if (indice > 0) strRes += ",";
                    strRes += ConvertirValorHorarioACadena(valores[indice]);
                    indice = i;
                }
                if (i == 23)
                {
                    if (indice > 0) strRes += ",";
                    strRes += ConvertirValorHorarioACadena(valores[23]);
                }
            }
            return strRes;
        }
        public static double ParseValorHorario(string valor, double valordefecto=0.0)
        {
            double d = valordefecto;
            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-GB");
            double.TryParse(valor, System.Globalization.NumberStyles.AllowDecimalPoint, culture, out d);
            return d;
        }
        public static string ConvertirValorHorarioACadena(double valor)
        {
            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-GB");
            System.Globalization.NumberFormatInfo n = culture.NumberFormat;
            IFormatProvider p = n;
            return valor.ToString("0.00", p);
        }

        public static bool GenerarArchivoCSV(List<CSchedSet> horarios, StreamWriter archivo, DateTime fechaDesde, DateTime fechaHasta, bool NotacionExp = false, int NumeroDecimales = 4, bool ColumnaUnos = true)
        {
            int iNumDec;
            string strFormato = "";
            try
            {
                //1º Crear una lista con los valores double de los horarios. Si ColumnaUnos==true, añadimos un horario "null" para que rellene con unos
                List<CSchedSet> lista = new List<CSchedSet>(horarios);
                if(ColumnaUnos==true) lista.Insert(0, new CSchedSet());
                List<double[]> datos = ObtenerArrayDatos(lista, fechaDesde, fechaHasta, 1);

                //2º Cadena de formato de los números y cultura y primera columna llena de unos
                if ((NumeroDecimales<2) && (NotacionExp==true)) iNumDec=2; else iNumDec=NumeroDecimales;
                if (NotacionExp == true) 
                    strFormato = "E" + NumeroDecimales.ToString();
                else if (NumeroDecimales > 0)                    
                    strFormato += "0." + new String('0', iNumDec);

                IFormatProvider cultura = new System.Globalization.CultureInfo("en-US", true);
                
                
                //3ºIr escribiendo línea a línea en el stream todos los valores            
                int j = 0, k = datos.Count(), l=0;
                if(k>0) l = datos[0].Count();

                string strLinea="";
                while (j < l)
                {
                    strLinea = "";
                    for (int i = 0; i < k; i++)
                        if (i < k) strLinea += datos[i].ElementAt(j).ToString(strFormato, cultura) + "\t"; else strLinea += datos[i].ElementAt(j).ToString(strFormato, cultura);
                    archivo.WriteLine(strLinea);
                    j++;
                }
            }
            catch (Exception) { return false; }
            return true;
        }

        public static List<double[]> ObtenerArrayDatos(List<CSchedSet> horarios, DateTime fechaDesde, DateTime fechaHasta, double ValorNull = 1)
        {
            List<double[]> datos = new List<double[]>();
            foreach (CSchedSet s in horarios)
            {
                if(s.NumeroDeHorarios == 0 )
                    datos.Add(Enumerable.Repeat<double>(ValorNull, (int) fechaHasta.Subtract(fechaDesde).TotalHours).ToArray<double>());
                else
                    datos.Add(s.GetValoresHorarios(fechaDesde, fechaHasta));
            }
            return datos;                
        }
    }
}
