﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleControls
{
    public sealed class SchedTypeEnum
    {
        private readonly String name;
        private readonly int value;

        public static readonly SchedTypeEnum INFILTRACION = new SchedTypeEnum(1, "INFILTRACION");
        public static readonly SchedTypeEnum OCUPACION = new SchedTypeEnum(2, "OCUPACION");
        public static readonly SchedTypeEnum CONSIGNA_CALEFACCION = new SchedTypeEnum(3, "CONSIGNA_CALEFACCION");
        public static readonly SchedTypeEnum POTENCIA_CALEFACCION = new SchedTypeEnum(4, "POTENCIA_CALEFACCION");
        public static readonly SchedTypeEnum CONSIGNA_REFRIGERACION = new SchedTypeEnum(5, "CONSIGNA_REFRIGERACION");
        public static readonly SchedTypeEnum POTENCIA_REFRIGERACION = new SchedTypeEnum(6, "POTENCIA_REFRIGERACION");

        private SchedTypeEnum(int value, String name)
        {
            this.name = name;
            this.value = value;
        }
        public override String ToString()
        {
            return name;
        }
    }
}
