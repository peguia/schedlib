﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScheduleControls
{
    /// <summary>
    /// Interaction logic for SchedGridCtrl.xaml
    /// </summary>
    /// FALTA: cuando sea muy pequeño debería quitar números de horas y del ejeY
    /// FALTA: Que se pueda introducir un valor en el textbox de encima de las horas. Cuando le den con el botón derecho a un schedhourctrl, que pida el valor con un inputbox.
    /// FALTA: Que devuelva del alguna manera una cadena de persistencia. Quizá 24 valores separados por comas
    /// FALTA: Que si le doy doble click a una hora, se ponga en modo edición esa hora y arrastre a los vecinos (todos, izquierda o derecha)
    /// FALTA: Que se le pueda aplicar un color diferente según el tipo de horario (diario, sábado, domingo, findesemana, uno para cada día (7 colores), festivos)->Control calendario
    /// FALTA: incluso que se pueda dar a Esc y revertir el cambio (tan fácil como ir guardando el estado anterior)
    /// FALTA: Aplicar un ControlTemplate a un LineaResizear -  ver el ejemplo de los thumbs.
    public partial class SchedGridCtrl : UserControl
    {
        List<SchedHourCtrl> MisSchedValues;
        
        double _MinY = 0.0, _MaxY = 1.0;
        bool bResizeandoVecinos = false;
        double[] MyValores = null;
        private bool _bModoEdicion = false;
        private bool _bEditable = true;

        SchedSetCtrl MiSchedSetCtrl;
        public CSchedSet MiSchedSet;

        public SchedGridCtrl()
        {
            InitializeComponent();
            MisSchedValues = MiGrid.Children.OfType<SchedHourCtrl>().ToList();
            InicializarControles();
        }

        private void InicializarControles() 
        {
            if (MiSchedSet != null) SetMaxYMinY(MiSchedSet.MaxValue, MiSchedSet.MinValue);
            EtiquetarEjeY();

            for(int i = 0; i<MisSchedValues.Count; i++)
            {   SchedHourCtrl s = MisSchedValues[i];
                s.SetGrid(this);
                s.SetMaxMinValues(_MaxY, _MinY);
            }
        }
        public bool Editable { get { return _bEditable; } set { _bEditable = value; } }
        //Si no tiene horarios, debemos borrar MyValores y quitar lo que haya en pantalla
        public void SetSchedSet(SchedSetCtrl s) 
        { 
            MiSchedSetCtrl = s; MiSchedSet = s.ActualSchedSet; 
            this.DataContext = MiSchedSet; 
            InicializarControles(); 
        }
        public void SetMaxYMinY(double maximo, double minimo)
        {
            _MinY = minimo; _MaxY = maximo;
            foreach (SchedHourCtrl s in MisSchedValues)
            {
                s.SetMaxMinValues(maximo, minimo);
            }
        }
        public void SetValoresHorarios(double[] valores)
        {
            MyValores = (double[]) valores.Clone();
            ActualizarValoresHorarios();
        }
        public void SetValoresHorarios(CSchedule s)
        {
            if (s == null)
            {
                MyValores = null;
                BorrarValoresHorarios();
                return;
            }
            MyValores = (double[]) s.GetValoresHorarios().Clone();
            ActualizarValoresHorarios();
        }
        public void SetValoresHorarios(double d) 
        {
            MyValores = Enumerable.Repeat<double>(d, 24).ToArray<double>();
            ActualizarValoresHorarios();
            if (MiSchedSetCtrl != null) MiSchedSetCtrl.ActualizarValoresHorario(this);
            
        }
        //A veces cambian los valores de los SchedHourCtrl por código (por ejemplo) y debemos actualizar el array de 24 valores en ese momento.
        public void LeerValoresControles()
        {
            for (int i = 0; i < 24; i++) { MyValores[i] = MisSchedValues.ElementAt(i).GetValor(); }
        }
        public double[] GetValoresHorarios()
        {
            return (double[]) MyValores.Clone(); 
        }
        public void BorrarValoresHorarios()
        {
            for (int i = 0; i < 24; i++)
            {
                MisSchedValues.ElementAt<SchedHourCtrl>(i).SetValor(0,false);
            }
        }
        private void ActualizarValoresHorarios()
        {
            if (MyValores==null) return;

            for(int i=0;i<24; i++) {
                MisSchedValues.ElementAt<SchedHourCtrl>(i).SetValor(MyValores[i],false);
            }
        }

        public void ShowResizeAdorner(int hora=-1)
        {
            if (hora == -1)
            {
                foreach(SchedHourCtrl s in MisSchedValues)
                {
                    s.ShowResizeAdorner();
                }
            }
            else
            {
                SchedHourCtrl s = MisSchedValues.ElementAt(hora);
                s.ShowResizeAdorner();
            }
        }
        public void HideResizeAdorner(int hora=-1)
        {
            if (hora == -1)
            {
                foreach (SchedHourCtrl s in MisSchedValues)
                {
                    s.HideResizeAdorner();
                }
            }
            else
            {
                SchedHourCtrl s = MisSchedValues.ElementAt(hora);
                s.HideResizeAdorner();
            }
        }
        //Si lado=1 dice que hacia la izquierda, si es 2 hacia la derecha. Si es 0, los vecinos son hacia los 2 lados.
        private IEnumerable<SchedHourCtrl> GetVecinosAcoplados(SchedHourCtrl h, int lado=0)
        {
            List<SchedHourCtrl> resultado = new List<SchedHourCtrl>();
            int i = MisSchedValues.IndexOf(h);
            int j, k;
            double dblValor = h.GetValor();
            for (j = i; j >= 0; j--) { if (MisSchedValues.ElementAt<SchedHourCtrl>(j).GetValor() != dblValor) break; } j++;
            for (k = i; k < 24; k++) { if (MisSchedValues.ElementAt<SchedHourCtrl>(k).GetValor() != dblValor) break; } k--;
            for (int l = j; l <= k; l++) 
            { if (((l < i)&&((lado==0)||(lado==1))) || ((l > i)&&((lado==0)||(lado==2)))) resultado.Add(MisSchedValues.ElementAt(l)); 
            }
            return resultado;
        }
        public void ResizearVecinos(SchedHourCtrl h, double deltaHeight, int lado = -1)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) return;
            if (bResizeandoVecinos == false)
            {
                int i;
                bResizeandoVecinos = true;
                if(lado==-1)
                {   i = 0; if (Keyboard.IsKeyDown(Key.LeftShift)) i = 1; if (Keyboard.IsKeyDown(Key.RightShift)) i = 2; }
                else i = lado;

                IEnumerable<SchedHourCtrl> vecinos = GetVecinosAcoplados(h, i);
                foreach (SchedHourCtrl s in vecinos)
                {
                    s.MiLineaResizear.ResizearThumb(deltaHeight);
                }
                bResizeandoVecinos = false;
            }
            
        }
        public void SetHeightVecinos(SchedHourCtrl h, double newHeight, int lado=-1)
        {
            if (bResizeandoVecinos == false)    //No está muy bien hecho: pero si uno empieza a resizear a vecinos, hasta que acaba no hay vecino que valga.
            {
                bResizeandoVecinos = true;
                IEnumerable<SchedHourCtrl> vecinos = GetVecinosAcoplados(h, lado);
                foreach (SchedHourCtrl s in vecinos)
                {
                    s.SetValor(newHeight);
                }
                bResizeandoVecinos = false;
            }

        }

        private void MiGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ActualizarValoresHorarios();
        }
        private void EtiquetarEjeY()
        {
            double[] valores = Enumerable.Range(0, 11).Select(x => (double) (x*(_MaxY-_MinY)/10.0)+_MinY).Reverse().ToArray();
            List<TextBlock> MisLabelsY = GridEjeY.Children.OfType<TextBlock>().ToList();
            string strFormato = "0.00";
            if (_MaxY > 9999) strFormato = "0.00E+00";
            for (int i = 0; i <= 10; i++) MisLabelsY.ElementAt(i).Text = valores[i].ToString(strFormato);
        }
        public void ResizeHour_Start(SchedHourCtrl sender)
        {
            TextoAdornoDrag.Text = sender.GetValor().ToString("0.00");
            AdornoDrag.Visibility = Visibility.Visible;   
        }
        public void ResizeHour_Delta(SchedHourCtrl sender) 
        {
            TextoAdornoDrag.Text = sender.GetValor().ToString("0.00");
        }

        public void ResizeHour_Completed(SchedHourCtrl sender)
        {
            AdornoDrag.Visibility = Visibility.Hidden;
            LeerValoresControles();
            if (MiSchedSetCtrl != null) MiSchedSetCtrl.ActualizarValoresHorario(this);
        }

        private void BtnPropiedadesSched_Click(object sender, RoutedEventArgs e)
        {
            CSchedSet s = MiSchedSetCtrl.ActualSchedSet;
            SchedOptionsWindow w = new SchedOptionsWindow(s);
            w.Left = BtnPropiedadesSched.PointToScreen(new Point(0, 0)).X + BtnPropiedadesSched.ActualWidth;
            w.Top = BtnPropiedadesSched.PointToScreen(new Point(0, 0)).Y - w.Height;
            if (w.ShowDialog() == true)
                s.Copiar(w.MiSchedSet, false);
        }
        public void SetNumeroStyle(int numero)
        {
            Style s;
            switch (numero % 3)
            {
                case 1: s = this.FindResource("SchedHourStyleAzul") as Style; break;
                case 2: s = this.FindResource("SchedHourStyleAlegre") as Style; break;
                case 0: s = this.FindResource("SchedHourStyleRojo") as Style; break;
                default: s = this.FindResource("SchedHourStyleAzul") as Style; break;

            }

            foreach (SchedHourCtrl h in MisSchedValues)  { h.SetSchedHourStyle(s); }
        }
        public void ToggleEditar()
        {
            if ((_bEditable == false)||(MyValores==null)) { HideResizeAdorner(); _bModoEdicion = false; return; }
            
            if (_bModoEdicion == true) {HideResizeAdorner(); _bModoEdicion = false; }
            else {ShowResizeAdorner(); _bModoEdicion = true; }
        }

    }
}
